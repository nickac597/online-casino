﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
	public void GoToMainScene(){
		SceneManager.LoadScene("MainMenu");
	}
	
	public void GoToSelectGameScene(){
		SceneManager.LoadScene("SelectGame");
	}

	public void GoToSettingsScene()
    {
		SceneManager.LoadScene("Settings");
    }
	
	public void GoToBlackJackScene(){
		SceneManager.LoadScene("BlackJack");
	}
	
	public void QuitApp(){
		Application.Quit();
	}
}