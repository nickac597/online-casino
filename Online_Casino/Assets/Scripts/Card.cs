﻿using System;

public class Card
{
    public string CardValueFace { get; set; }
    public string CardSuite { get; set; }
    public int CardValueNum { get; set; }
    public string ImgPath { get; set; }
    string RealValueString { get; set; }


    public Card(string cardValueFace, string suite, int cardValueNum, string realValueString)
    {
        CardValueFace = cardValueFace;
        CardSuite = suite;
        CardValueNum = cardValueNum;
        RealValueString = realValueString;
        ImgPath = String.Concat(@"Assets\CommonAssets\Images\CardFaces\Classic\", CardValueFace,CardSuite,RealValueString,".png");
    }
}