﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Diagnostics;

public class BlackJack : MonoBehaviour
{
    public Text pHandfield;
    public Text pHandfield2;
    public Text pHandfield3;
    public Text pHandfield4;
    public Text pHandfield5;
    public Text pHandfield6;
    public Text pHandield7;
    public Text pHandfield8;
    public Text pHandield9;
    public Text pHandfield10;
    public Text pHandText11;

    public Text dHandfield;
    public Text dHandfield2;
    public Text dHandfield3;
    public Text dHandfield4;
    public Text dHandfield5;
    public Text dHandfield6;
    public Text dHandield7;
    public Text dHandfield8;
    public Text dHandield9;
    public Text dHandfield10;
    public Text dHandText11;

    public Text PlayerHandSize;
    public Text DealerHandSize;
    public Text PHandValue;
    public Text DHandValue;

    public Stack ShuffleDeck;
    public Image CardImg;
    public List<Card> PlayerHand = new List<Card>();
    public List<Card> DealerHand = new List<Card>();
    public int PlayerCardno = 0;
    public int DealerCardno = 0;
    public int PlayerHandValue = 0;
    public int DealerHandValue = 0;

    public UnityEngine.Random rnd = new UnityEngine.Random();

    public void Start()
    {
        
        GenerateDeck();
    }

    public void Deal()
    {
        for (int i = 0;i <= 1;i++)
        {
            PlayerHand.Add(getCard());
            PlayerCardno += 1;
            PlayerHandSize.text = (PlayerCardno+"");
            PlayerHandValue += PlayerHand[i].CardValueNum;
        }
        pHandfield.text = (PlayerHand[0].CardValueFace + " of " + PlayerHand[0].CardSuite);
        pHandfield2.text = (PlayerHand[1].CardValueFace + " of " + PlayerHand[1].CardSuite);
        PHandValue.text = (PlayerHandValue + "");

        for (int i = 0; i <= 1; i++)
        {
            DealerHand.Add(getCard());
            DealerCardno += 1;
            DealerHandSize.text = (DealerCardno + "");
            DealerHandValue += DealerHand[i].CardValueNum;
        }
        dHandfield.text = (DealerHand[0].CardValueFace + " of " + DealerHand[0].CardSuite);
        dHandfield2.text = (DealerHand[1].CardValueFace + " of " + DealerHand[1].CardSuite);
        DHandValue.text = (DealerHandValue + "");
    }

    public void SetImg(string path)
    {
        
        Sprite myImage = Resources.Load<Sprite>(path); 
        CardImg.sprite = myImage;
    }

    public void GenerateDeck()
    {
        UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks);
        PlayerCardno = 0;
        DealerCardno = 0;
        PlayerHandValue = 0;
        DealerHandValue = 0;

        while (PlayerHand.Count > 0)
        {
            PlayerHand.Clear();
        }
        while (DealerHand.Count > 0)
        {
            DealerHand.Clear();
        }


        Stack shuffleDeck = new Stack();
        List<Card> unShuffleDeck = new List<Card>();

        string[] lines = System.IO.File.ReadAllLines(@".\Assets\JudeAssets\blackJackCards.txt");
        foreach (string line in lines)
        {
            String faceValue = line.Substring(0, 2);


            String faceSuite;
            if (String.Equals(line.Substring(2, 1), "h"))
            {
                faceSuite = "h";
            }
            else if (String.Equals(line.Substring(2, 1), "c"))
            {
                faceSuite = "c";
            }
            else if (String.Equals(line.Substring(2, 1), "d"))
            {
                faceSuite = "d";
            }
            else
            {
                faceSuite = "s";
            }

            string realValueString = line.Substring(3);
            int realValue = int.Parse(line.Substring(3));

            Card newCard = new Card(faceValue, faceSuite, realValue, realValueString);
            unShuffleDeck.Add(newCard);
        }

        int iRand;

        while (unShuffleDeck.Count > 0)
        {
            iRand = UnityEngine.Random.Range(0,unShuffleDeck.Count);
            shuffleDeck.Push(unShuffleDeck[iRand]);
            unShuffleDeck.RemoveAt(iRand);
        }

        ShuffleDeck = shuffleDeck;
        Deal();
    }

    public Card getCard()
    {
        Stack Deck = ShuffleDeck;
        Card topCard = (Card)Deck.Pop();
        return topCard;
    }
}
