﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    //could not find a way to create a global object that each method can use, so a local one is created in each method.
    public class Scenario1Test
    {

      public string sceneVal1 = "initial";
        //Go to main menu
        [UnityTest]
        public IEnumerator MainMenuTest()
        {
            var gameObject = new GameObject();
            var loader = gameObject.AddComponent<LoadScene>();
            sceneVal1 = loader.GoToMainScene();
            Assert.AreEqual("Main Menu", sceneVal1);
            yield return null;
        }
        //Go to select a game screen
        [UnityTest]
        public IEnumerator SelectGameTest()
        {
            var gameObject = new GameObject();
            var loader = gameObject.AddComponent<LoadScene>();
            sceneVal1 = loader.GoToSelectGameScene();
            Assert.AreEqual("Select Game", sceneVal1);
            yield return null;
        }
        //select blackjack
        [UnityTest]
        public IEnumerator BlackJackTest()
        {
            var gameObject = new GameObject();
            var loader = gameObject.AddComponent<LoadScene>();
            sceneVal1 = loader.GoToBlackJackScene();
            Assert.AreEqual("BlackJack", sceneVal1);
            yield return null;
        }
        //--------------------------------------------------------------------------------------------------------------------------
        //Final playthrough is not complete and will not work, errors have not been fixed completely in the regular code.
        //Some code has been implemented to playthrough but will not run because of errors in main scripts
        //Play BlackJack
        // [UnityTest]
        // public IEnumerator Scenar1BlackJackTest()
        // {
        //  var gameObject = new GameObject();
        //var bjTest = gameObject.AddComponent<BlackJack>();
        // bjTest.GenerateDeck();
        // bjTest.getCard();
        // bjTest.getCard();
        //   yield return null;
        //}
    }
}
