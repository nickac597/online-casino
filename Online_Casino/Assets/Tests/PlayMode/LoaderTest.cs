﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class LoaderTest
    {
        public string sceneVal1 = "initial";
        [UnityTest]
        public IEnumerator MainMenuTest()
        {
            var gameObject = new GameObject();
            var loader = gameObject.AddComponent<LoadScene>();
            sceneVal1 = loader.GoToMainScene();
            Assert.AreEqual("Main Menu", sceneVal1);
            yield return null;
        }

        [UnityTest]
        public IEnumerator SelectGameTest()
        {
            var gameObject = new GameObject();
            var loader = gameObject.AddComponent<LoadScene>();
            sceneVal1 = loader.GoToSelectGameScene();
            Assert.AreEqual("Select Game", sceneVal1);
            yield return null;
        }

        [UnityTest]
        public IEnumerator SettingsTest()
        {
            var gameObject = new GameObject();
            var loader = gameObject.AddComponent<LoadScene>();
            sceneVal1 = loader.GoToSettingsScene();
            Assert.AreEqual("Settings", sceneVal1);
            yield return null;
        }
    }
}
