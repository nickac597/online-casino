# Project Title
Online Casino

## Author Name
Jude Eschete

## Getting Started
How to deploy the system.

### Installing Unity Hub:

Go to https://unity3d.com/get-unity/download.

Log in or create an account if need. 

Click the “Download Unity Hub” button.

Run the downloaded .exe file. 

Run Unity Hub.

### Installing Correct Unity Version: 

In the Unity Hub under “Installs” On the left hand side	 

Click “Add” in the top right.

In the popup window, click “Visit our download archive”

On the webpage that appears click on the Unity 2019.X tab and scroll to the Unity 2019.4.10 choice and click the green “Unity Hub” button. 

Check any options on the screen that pops up in the unity hub relating to Visual Studio and click install. 


### Adding Project to Unity: 

Clone project from git repository.

In the Unity Hub section “Projects”, click the “Add” in the top right and point to the folder “Online_Casino” in the project folder.

Click on the project and wait for Unity to set up the environment. 

The environment is set up. 

To run the project, in the top left under “File” Click “Build and Run”.


### changes
Added installation instructions. 